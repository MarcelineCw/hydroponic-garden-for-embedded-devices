#include <EEPROM.h>

//TARGET is a constant that contains a value corresponding to the arduino board we are targeting
//Target = 1 is the uno
//Target = 0 is the nano
//Currently both boards are using the same pinout
//Edit these pin numbers if you have connected the componenents to different pins. Remember only some have analogue in
#define TARGET 1
#ifdef TARGET < 2
  //Define a constant for the PH probe pin so we can read from it, its on Analogue 0
  const int PHPROBEPIN = 0;
  //Define a pin constant for the motors, they are refered to by the action they have on the water bath
  const int PH_UP_MOTOR = 1;
  const int PH_DOWN_MOTOR = 2;
  const int NUTRIENT_RELEASE_MOTOR = 3;
  //Define the pin for the calibration mode switch
  const int CALIBRATE_SWITCH = 0;
  //Define the pin for the error light, symbolizing calibration is needed
  const int ERROR_LED = 5;
#endif
//Set to 1 if you have a nutrient probe comment out if you don't
//#define NUTRIENTSYSTEM 1
#ifdef NUTRIENTSYSTEM
  const int NUTRIENT_PROBE_VCC = 6;
  const int NUTRIENT_PROBE_PIN = 1;
  const int NUTRIENT_THRESHOLD = 150;
#else
  int loopCounter = 0;
#endif
//Used to store the characteristics of the ph probe
short phGradient = 0;
short phConstant = 0;
//USER VARIABLES
//This controls the PH that the hydoponic garden is targeting LB is lower bound ub is upper bound
float TARGET_PH_LB = 6.5;
float TARGET_PH_UP = 6.8;
//Hours between nutrient delivery and length of nutrient delivery in seconds
const int HOURS_BETWEEN_FOOD = 24;
const int LENGTH_OF_FEEDING_S = 60;
//Strength of acid calibration fluid
const int ACID_CALIBRATION = 4;
//strength of basic calibration fluid
const int BASE_CALIBRATION = 10;

void setup() {
  //Initialize all the pins we defined above
  pinMode(A0,INPUT);
  pinMode(PH_UP_MOTOR, OUTPUT);
  pinMode(PH_DOWN_MOTOR, OUTPUT);
  pinMode(NUTRIENT_RELEASE_MOTOR, OUTPUT);
  pinMode(CALIBRATE_SWITCH, INPUT);
  pinMode(ERROR_LED, OUTPUT);
  ph_Retrieve_Calibration(); 
}

void loop() {
  // put your main code here, to run repeatedly:
  //Read the current value of the ph probe in the water
  float currentPh = ph_Read();
  //If we are below the target ph range we will pour some ph up solution into the tank slowly until the ph comes within range
  while(currentPh < TARGET_PH_LB){
      ph_Up();
      currentPh = ph_Read();
  }
  //If we are above the target ph range we will pour ph down solution into the tank slowly until the ph comes within range
  while(currentPh > TARGET_PH_UP){
      ph_Down();
      currentPh = ph_Read();
  }
  //If there is a nutrient probe we use a feedback system to monitor the nutrient levels
  #ifdef NUTRIENTSYSTEM
    //Read the nutrient probe and if we are below our threshold we pump nutrients into the tank for a while
    float currentPPM = nutrient_Read();
    if(currentPPM < NUTRIENT_THRESHOLD)
    {
      nutrients_Up();
    }
  #else
  //If we don't have a nutrient probe we just add nutrients on a schedule
    loopCounter++;
    if(loopCounter/6 == HOURS_BETWEEN_FOOD){nutrients_Up(LENGTH_OF_FEEDING_S); loopCounter = 0;}
  #endif
  //Wait for 10 minutes to check the probe again.
  //This helps prevent any damage we might incur from overuse of the sensors
  delay(600000);
}

void error_Flash(){
  //Used to flash the led with a period of 200ms
  //This is used in the Calibration phase to let you know the program is ready for a calibration read
  for(int i=0;i<6;i++){
    digitalWrite(ERROR_LED,HIGH);
    delay(100);
    digitalWrite(ERROR_LED,LOW);
    delay(100);}
}

void rising_Edge_Blocking(int pin){
  //This is a function used to detect a rising edge at the given digital pin
  //It blocks the thread which calls it
  while(digitalRead(pin) == HIGH){delay(100);}
  while(digitalRead(pin) == LOW){delay(100);}
  return;
  }

short read_Short_EEPROM_BE(int startingAddress){
  //Reads a short from EEPROM big endian stylee
  byte buffer;
  short retShort;
  buffer = EEPROM.read(startingAddress);
  retShort |= (buffer<<8);
  buffer = EEPROM.read(startingAddress + 1);
  retShort |= buffer;
  return retShort;
}

void write_Short_EEPROM_BE(int addr, short val){
  //Writes a short to EEPROM big endian stylee
  byte buffer = (0xf0 & val)>>8;
  EEPROM.write(addr, buffer);
  buffer = 0x0f & val;
  EEPROM.write(addr, buffer);
}

void ph_Retrieve_Calibration(){
  //This retrieves the gradient and constant of our ph probe from memory or makes you calibrate it and stores it in memory
  //If your arduino has had things in its EEPROM before PLEASE CLEAR IT I've included a sketch called clear EEPROM that should clear it for you.
  //Here is a brief layout of our EEPROM
  //0x00 Byte that says if we have written our values if not then calibrate
  //0x01 0x02 big endian 16 bit value that corresponds to the gradient
  //0x03 0x04 big endian 16 bit value that corresponds to the constant
  byte buffer;
  buffer = EEPROM.read(0);
  //If the first address has never been written we need to calibrate
  if(buffer == 0xff){ph_Calibrate();return;}
  phGradient = read_Short_EEPROM_BE(1);
  phConstant = read_Short_EEPROM_BE(3);
}

void ph_Calibrate(){
  //This calibrates the PH meter by using calibration solutions which come with the probe
  //The probe I looked at came with solutions of 4 and 10 and then builds a linear relationship from those points
  //First we put the LED HIGH for 3 seconds and then flash it with a period of 200 ms for 1s to show calibration is needed
  digitalWrite(ERROR_LED,HIGH);
  delay(3000);
  error_Flash();
  //After flashing we turn the ERROR LED back on to show the user that it's ready to accept a reading
  digitalWrite(ERROR_LED,HIGH);
  //We wait until the user flicks the calibration switch. This lets us know they have put the probe in the correct solution
  rising_Edge_Blocking(CALIBRATE_SWITCH);
  //Read the result of the acid solution into a var
  int phSampleAcid = analogRead(PHPROBEPIN);
  //Repeat the light sequence to let them know to move onto the next solution
  error_Flash();
  digitalWrite(ERROR_LED, HIGH);
  //Wait for the switch signaling the probe is in the base solution
  rising_Edge_Blocking(CALIBRATE_SWITCH);
  //Read the result of the base solution
  int phSampleBase = analogRead(PHPROBEPIN);
  //Now we solve the simultaneous equations from y = mx + c to find the line of our ph probes results
  phGradient = (BASE_CALIBRATION - ACID_CALIBRATION)/(phSampleBase - phSampleAcid);
  phConstant = BASE_CALIBRATION - phGradient * phSampleBase;
  //Write the values to eeprom for further use as calibrating requires user effort and thus should be avoided
  write_Short_EEPROM_BE(1,(short)phGradient);
  write_Short_EEPROM_BE(2,(short)phConstant);
  }

float ph_Read(){
  //reads the ph and returns it as a float
  int buffer = analogRead(PHPROBEPIN);
  float ret_Ph = phGradient * buffer + phConstant;
  return ret_Ph;
}

void ph_Up(){
  //Turns on and then off the ph up motor
  digitalWrite(PH_UP_MOTOR, HIGH);
  delay(1000);
  digitalWrite(PH_UP_MOTOR, LOW);
  delay(10000);
}

void ph_Down(){
  //Turns on and then off the ph down motor
  digitalWrite(PH_DOWN_MOTOR, HIGH);
  delay(1000);
  digitalWrite(PH_DOWN_MOTOR, LOW);
  delay(10000);
}

void nutrients_Up(int feedTime){
  //Turns on the nutrient pump for the set time
  digitalWrite(NUTRIENT_RELEASE_MOTOR, HIGH);
  delay(feedTime * 1000);
  digitalWrite(NUTRIENT_RELEASE_MOTOR, LOW);
  delay(10000);
}

#ifdef NUTRIENTSYSTEM
  void nutrient_Read(){

  }
#endif
