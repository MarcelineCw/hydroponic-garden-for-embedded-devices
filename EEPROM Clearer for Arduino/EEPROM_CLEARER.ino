#include <EEPROM.h>

//ONLY RUN THIS IF YOU WANT THE EEPROM OF YOUR ARDUINO SET TO 0xFF noting UNWRITTEN
//This is to avoid errors where we store the calibration settings for the PH probe in EEPROM
//and prevent data that has been stored in EEPROM by other arduino programs from corrupting our functionality
void setup() {
  int a=0;
  while(a < EEPROM.length()){
    EEPROM.write(a,0xFF);
    }

}

void loop() {
  // put your main code here, to run repeatedly:

}
