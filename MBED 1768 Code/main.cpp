#include "mbed.h"
#include "rtos.h"
//Library used to write to the flash memory, https://os.mbed.com/users/okano/code/IAP/
#include "IAP.h"

//Use more readable values for digital pin outputs
#define HIGH 1
#define LOW 0
//Defines the pins that devices are connected to 
#define CALIBRATION_SWITCH p5
#define PHUPMOTOR p6
#define PHDOWNMOTOR p7
#define NUTRIENTUPMOTOR p8
#define LIGHTMULTIPLEX p9
#define ERRORLED p10
#define PHPROBEVCC p11
#define PHPROBEOUTPUT p15
//Define the ph of the calibration solutions
#define ACID_CALIBRATION 4.0
#define BASE_CALIBRATION 10.0
//Define the area of flash we are using to store calibration data
#define TARGET_SECTOR 29
//Define a type for uchar
typedef unsigned char byte; 

//Creates objects to control the pins we will be using
//Create objects for the digital pins controlling the motor multiplexes
DigitalOut phUpMotor(PHUPMOTOR) = LOW;
DigitalOut phDownMotor(PHDOWNMOTOR) = LOW;
DigitalOut nutrientUpMotor(NUTRIENTUPMOTOR) = LOW;
//Light multiplexer pin
DigitalOut lightMultiplex(LIGHTMULTIPLEX) = LOW;
//Error LED pin
DigitalOut errorLED(ERRORLED) = LOW;
//Setup the calibration switch as an interupt
InteruptIn calibrationSwitch(CALIBRATION_SWITCH);
calibrationSwitch.rise(&calibrationInterrupt);
calibrationSwitch.disable_irq();
//Setup the pins for the PH probe
DigitalOut phProbeVCC(PHPROBEVCC) = LOW;
AnalogIn phProbeOutput(PHPROBEOUTPUT);

IAP iap;

float phGradient;
float phConstant;

int main()
{
	while(1){
		//Main loop of program
		float currentPh = phRead();
	}
	return 0;
}

void setup(){
	//Function that provides a space to setup things
	calibrationSwitch.enable_irq();
}

void errorFlash(){
	//Flashes the error LED with a period of 200 ms
	for (int i = 0; i < 6; ++i)
	{
		errorLED = HIGH;
		Thread::wait(100);
		errorLED = LOW;
		Thread::wait(100);
	}
}

float getFloat(byte* array){
	//Pass this an address of where in a char array you want this float to be read from NOTE UNDEFINED BEHAVIOUR IF PASSED THE END OF AN ARRAY
	//Returns a float from an array of bytes
	float buffer = 0;
	float *f_array = (float*) array;
	buffer = f_array[0];
	return buffer;
}

void writeFloat(byte* array, float val){
	//Pass this an address of where in achar array you want a short to be written to NOTE UNDEFINED BEHAVIOUR IF PASSED THE END OF AN ARRAY
	//Writes a float into an array of bytes
	float *f_array = (float*)array;
	
}

void phCalibrateAndStore(){
	//We need to calibrate the values in our globals first
	calibrationInterrupt();
	//phCalibrate puts the right values in our global variables we just need to store them in flash to make sure we keep them for next time
	byte var_storage[2*sizeof(float)];
	writeFloat(&var_storage,phGradient);
	writeFloat(&var_storage+sizeof(float),phConstant);
	iap.prepare(TARGET_SECTOR, TARGET_SECTOR);
	iap.write(var_storage,sector_start_address[TARGET_SECTOR], 2*sizeof(float));
}

void readPhCalibration(){
	//Reads the previous ph calibration from non volatile memory
	int r;
	int breakLoop = 1;
	while(breakLoop){
		r = iap.blank_check(TARGET_SECTOR, TARGET_SECTOR);
		switch(r){
			case SECTOR_NOT_BLANK:
			//if the sector is not blank we need to retrieve and use the data within
			byte sector_contents[256];
			memcpy(sector_contents,sector_start_address[TARGET_SECTOR],256 * sizeof(byte))
			phGradient = getBEFloat(&sector_contents[0]);
			phConstant = getBEFloat(&sector_contents + sizeof(float));
			breakLoop = 0;
			break;
			case CMD_SUCCESS:
			//If the sector is blank then we need to calibrate and store the calibration in it
			phCalibrateAndStore();
			breakLoop = 0;
			break;
			case default:
			//If the sector is invalid or the flash memory was busy try again
			break;
		}
	}
	
}

void calibrationInterrupt(){
	//Forces a calibration to happen via interrupt when the calibration switch is push high
	//We disable the interrupt so that we can use the switch for other purposes within the interupt handler
	calibrationSwitch.disable_irq();
	//We call our ph calibration function to calibrate the ph probe
	phCalibrate();
	calibrationSwitch.rise(&calibrationInterrupt);
	calibrationSwitch.enable_irq();
}

void risingEdgeBlockingIntr(InteruptIn *pinPtr){
	//Takes an interrupt pin and waits for a rising edge on it then returns to the calling function
	while(pinPtr -> read() == HIGH){wait_ms(100);}
	while(pinPtr -> read() == LOW){wait_ms(100);}
	return;
}

void phCalibrate(DigitalIn *cSwitchPin){
	//Calibrates the PH Probe leaving new values in phGradient and phConstant
	//Set the error LED to high to signify we are entering calibration
	errorLED = HIGH;
	//Leave high for three seconds
	Thread::wait(3000);
	//Flash the light to show we are expecting a read soon
	errorFlash();
	//Leave the LED HIGH to show that we are ready to read the acid solution
	errorLED = HIGH;
	//Wait until the user presses the switch to let us know the calibration solution is in the probe
	risingEdgeBlockingIntr(&calibrationSwitch);
	//Turn the ph probe on
	phProbeVCC = HIGH;
	//Wait for one second to allow the ph probe to adjust
	wait(1.0);
	//Read the acid value from the ph probe
	unsigned short phSampleAcid = phProbeOutput.read_u16();
	//Repeat the LED signifier sequence to show we are moving on to the next solution
	errorFlash();
	errorLED = HIGH;
	//Wait for the user to press the switch to let us know the base calibration fluid is in the probe
	risingEdgeBlockingIntr(&calibrationSwitch);
	unsigned short phSampleBase = phProbeOutput.read_u16();
	phProbeVCC = LOW;
	//Solve the simultaineous equations to discover our calibration
	phGradient = (BASE_CALIBRATION - ACID_CALIBRATION)/(phSampleBase - phSampleAcid);
	phConstant = BASE_CALIBRATION - phGradient * phSampleBase;
}

float phRead(){
	//reads the PH probe and calculates the ph value from the raw data
	phProbeVCC = HIGH;
	wait(1.0);
	unsigned short buffer = phProbeOutput.read_u16();
	phProbeVCC = LOW;
	float retVal = phGradient * (float) buffer + phConstant;
	return retVal;
}

void phUp(){
	//turns on the ph up motor for a set time to raise the ph of the tank
	phUpMotor = HIGH;
	Thread::wait(1000);
	phUpMotor = low;
	Thread::wait(10000);
}

void phDown(){
	//turns on the ph down motor for a aset time to lower the ph of the tank
	phDownMotor = HIGH;
	Thread::wait(1000);
	phDownMotor = low;
	Thread::wait(10000);
}

void nutrientsUp(float feedTime){
	//turns on the nutrients motor for a set time to raise the nutrient levels in the tank
	phUpMotor = HIGH;
	wait(feedTime);
	phUpMotor = low;
	Thread::wait(10000);
}